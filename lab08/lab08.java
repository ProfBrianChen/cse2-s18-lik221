///Lisa Kestelboym 
///CSE 2- Lab 8 
///


import java.util.Scanner;
import java.util.Random;
public class lab08 {
  public static void main(String[] args){
    Scanner input = new Scanner(System.in);
    
    int numberOfStudents = (int)((Math.random() * 6) + 5);
    
    String[] students;
    students = new String[numberOfStudents];
    
    int[] midterm;
    midterm = new int[numberOfStudents];
    
    for(int i = 0; i < numberOfStudents; i++){
      System.out.print("Give us a student Name: ");
      students[i] = input.next(); 
    }
      
    for(int j = 0; j < numberOfStudents; j++){
      int testScore = (int)((Math.random() * 100) + 1);
      midterm[j] = testScore;
      System.out.println(students[j] + ": " + midterm[j]);
    }
    
  }
}
