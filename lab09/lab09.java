///Lisa Kestelboym 
///CSE 2 - lab 09 - passing arrays to methods 



public class lab09{
  
  public static int[] copy(int[] array){
    
    int[] newArray;
    newArray = new int[array.length];
    
    for(int i = 0; i < array.length; i++){
      newArray[i] = array[i];
    }// for loop to copy elements into new array
    
    return newArray;
    
  }// copy method 
  
  public static void inverter(int[] array){
    
    int temp = 0;
    for(int k = 0; k < array.length/2 ; k++){
      temp = array[array.length - k - 1];
      array[array.length - k - 1] = array[k];
      array[k] = temp;
    } // for loop
    
  }// invertor method to reverse the order of the array 
  
  public static int[] inverter2(int[] array){
    
    int[] flipArray = copy(array);
    inverter(flipArray);
    return flipArray;
    
  }// inverter 2 method 
  
  public static void print(int[] array){
    
    System.out.print("{ ");
    for(int j = 0; j < array.length; j++){
      System.out.print(array[j] + ", ");
    }// for loop to print out elements in array
    System.out.print("}");
    
  }// print method 
  
  public static void main(String[] args){
    int[] array0 = { 1, 2, 3, 4, 5, 6, 7, 8 };
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    
    inverter(array0);
    print(array0);
    System.out.println(" ");
 
    inverter2(array1);
    print(array1);
    System.out.println(" ");
 
    int[] array3 = inverter2(array2);
    print(array3);
    System.out.println(" ");
    
    
  }//main method 
  
  
}//class

