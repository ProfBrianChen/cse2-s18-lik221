//////////////////////////////////////////////
///Lisa Kestelboym
///CSE 2 - HW 07
///March 27, 2018 
///This program calculates the area of three different shapes, a rectangle, a triangle, and a circle.
///User chooses which shape to use and enters the dimensions of the shape they selected. 

import java.util.Scanner; //importing Scanner 

public class Area{ //declaring class
  public static void main(String[] args){ //declaring method 
    Scanner input = new Scanner( System.in ); //declaring Scanner called "input"
    
    System.out.print("Type 'rectangle', 'triangle', or 'circle' (without capital letters) depending on which shape you want to find the area of: "); 
    //prompting user to select which shape they want with a string variable 
    
    String shape = input.next(); //taking in user input about shape preference 
    //declaring string variables for the shape type 
    String rectangle = "rectangle";  
    String triangle = "triangle"; 
    String circle = "circle"; 

    //while statement that requires user to enter the exact spelling of the string 
     while ((!shape.equals(rectangle)) && !(shape.equals(triangle)) && !(shape.equals(circle))){
      System.out.print("This isn't a valid entry. Please enter rectangle, triangle, or circle: ");
      shape = input.next();
            }
    
    
    //if the user input is rectangle, asks for dimensions of the rectangle 
    //makes sure that the input is of type double
    while (shape.equals(rectangle)){
      double rectHeight = 0; 
      double rectWidth = 0;
      System.out.println("You have selected to find the area of a " + rectangle);
      System.out.print("Enter the rectangle's height: ");
    //while statement that confirms that the user input is of type double 
      while (true){
      if (input.hasNextDouble() == false){ 
        System.out.println("Invalid input. Enter an double: ");
        String junkWord = input.next();  
      }
      else if (input.hasNextDouble() == true){ 
        rectHeight = input.nextDouble();
        break;
      }
      }
      System.out.print("Enter the rectangle's width: ");
      while (true){
        if (input.hasNextDouble() == false){ 
        System.out.println("Invalid input. Enter an double: ");
        String junkWord = input.next();
      }
        else if (input.hasNextDouble() == true){ 
        rectWidth = input.nextDouble();
        break;
      }
      }
      rectArea(rectHeight, rectWidth);
      break;
    }
    
    //if the user input is triangle, asks for dimensions of a triangle 
    //makes sure that the input is of type double 
    while (shape.equals(triangle)){
      shape = triangle;
      double triHeight = 0; 
      double triBase = 0; 
      System.out.println("You have selected to find the area of a " + triangle); 
      System.out.print("Enter the triangle's height: ");
      while (true){
        if (input.hasNextDouble() == false){ 
        System.out.println("Invalid input. Enter an double: ");
        String junkWord = input.next(); 
      }
      else if (input.hasNextDouble() == true){  
        triHeight = input.nextDouble();
        break;
      }
      }
      System.out.print("Enter the length of the triangle's base: ");
      //while statement that confirms that the user input is of type double 
      while (true){
      if (input.hasNextDouble() == false){ 
        System.out.println("Invalid input. Enter an double: ");
        String junkWord = input.next(); 
      }
      else if (input.hasNextDouble() == true){ 
        triBase = input.nextDouble();
        break;
      }
      }
      triArea(triHeight, triBase);
      break;
    }
    
    
    
    //if the user input is circle, asks for dimensions of a circle 
    //make sure that the input is of type double 
    while (shape.equals(circle)){
      shape = circle;
      double radius = 0; 
      System.out.println("You have selected to find the area of a " + circle);
      System.out.print("Enter the circle's radius: ");
     //while statement that confirms that the user input is of type double 
      while (true){
        if (input.hasNextDouble() == false){ 
          System.out.println("Invalid input. Enter an double: ");
          String junkWord = input.next();
      }
        else if (input.hasNextDouble() == true){ 
          radius = input.nextDouble();
          break;
        }
      }
      circleArea(radius);
      break;
    }
    }
  
  
     //calculating the area of a rectangle 
  public static double rectArea (double rectHeight, double rectWidth){
    double rectArea = rectHeight * rectWidth; 
    System.out.println("A rectangle with a height of " + rectHeight + " and a width of " + rectWidth + ", the area is " + rectArea + ".");
    return rectArea;
    }
  
    //calculating the area of a triangle 
    public static double triArea (double triHeight, double triBase){
      double triArea = 0.5 * triHeight * triBase;
      System.out.println("A triangle with a height of " + triHeight + " and a base of " + triBase + ", the area is " + triArea + ".");
      return triArea;
      } 
   
    //calculating the area of a circle 
    public static double circleArea (double radius){
      double circleArea = Math.PI * radius * radius; 
      System.out.println(circleArea);
      return circleArea;
    }

    }