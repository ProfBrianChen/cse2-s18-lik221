//////////////////////////////////////////////
///Lisa Kestelboym
///CSE 2 - HW 07
///March 27, 2018 
///This program processes string variables by examining all characters and determining if they are letters. 
///The user can choose either or not they want to examine the entire string or just a certain number of characters 
import java.util.Scanner;

public class StringAnalysis{
  
  public static void main (String[] args){
    Scanner myScanner1 = new Scanner ( System.in); 
    System.out.print("Enter your string variable: ");
    String b = myScanner1.next(); 
    System.out.println("Enter 1 if you want to analyze the entire string and 2 if you want to enter a certain number of characters to analyze: ");
    int userDecision = myScanner1.nextInt(); 
    if (userDecision == 1){
      stringAnalysis (b);
    }
    else if (userDecision == 2 ){
      System.out.print ("Enter your string variable again: ");
      String y = myScanner1.next();
      System.out.println("Enter the number of characters you want to analyze in the string: ");
      int lengthEvaluation = myScanner1.nextInt();
      stringAnalysis (y, lengthEvaluation);
    }
  }
  
  public static boolean stringAnalysis (String a){
    int i = 0;
    int characterCount = a.length(); 
    if (Character.isLetter(a.charAt(i))){
      if (i <= characterCount){
        i++;
        System.out.println("All characters in the string are letters.");
      }
       
    }
    else {
        System.out.println("Not all of the characters in the string are letters.");
      
  }
    return true;
}
  
  public static boolean stringAnalysis (String e, int lengthSelection){
    int j = 0;
    int characterCount1 = e.length();
    if (j >= lengthSelection){
      if (Character.isLetter(e.charAt(j))){
        j++;     
        System.out.println("All characters in selected length are letters.");
      }
      else {
        System.out.println("Not all of the characters in the selected length are letters.");
      }
    }
    return true;
  }
    
}