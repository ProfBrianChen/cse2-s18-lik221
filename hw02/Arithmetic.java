///////////////////
///CSE2 Arithmetic 
///
  public class Arithmetic {
    
      public static void main(String[] args) {
            //Computes the cost of bought items including the PA 6% sales tax.  
//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;

        //Total costs of each item 
//Total cost of pants 
double totalCostofPants;
totalCostofPants = numPants * pantsPrice; 
  
//Total cost of sweatshirts 
double totalCostofSweatshirts;
totalCostofSweatshirts = numShirts * shirtPrice;
  
//Total cost of belts 
double totalCostofBelts;
totalCostofBelts = numBelts * beltCost;
        
        //Sales tax charged buying all of each kind of item
 //Sales tax charged on pants 
 double salesTaxOnPants;
 salesTaxOnPants = (totalCostofPants * paSalesTax); 
        
 //Sales tax charged on sweatshirts 
 double salesTaxOnSweatshirts ;
 salesTaxOnSweatshirts = (totalCostofPants * paSalesTax);
        
 //Sales tax charged on belts 
 double salesTaxonBelts;
 salesTaxonBelts = (totalCostofBelts * paSalesTax); 
        
        //Total cost of all items before tax 
 double totalCostofPurchasesBeforeTax; 
 totalCostofPurchasesBeforeTax = totalCostofBelts + totalCostofSweatshirts + totalCostofPants; 
        
        //Total sales tax 
double totalSalesTax; 
totalSalesTax = salesTaxonBelts + salesTaxOnSweatshirts + salesTaxOnPants; 
        
        //Total paid for the transaction including sales tax 
 double totalCostofPurchasesWithTax; 
 totalCostofPurchasesWithTax = totalCostofPurchasesBeforeTax + totalSalesTax;

        //Prints the total cost of each item        
System.out.println("The total cost of pants is" + " " + totalCostofPants);
System.out.println("The total cost of sweatshirts is" + " " + totalCostofSweatshirts);
System.out.println("The total cost of belts is" + " " + totalCostofBelts);
       
        //Prints the total amount paid before sales tax. 
System.out.println ("The total amount paid before sales tax is" + " " + totalCostofPurchasesBeforeTax); 
  
        //Prints the total sales tax paid for buying all of the items. 
System.out.println ("The total sales tax paid for buying these items is" + " " + (int) (totalSalesTax * 100) / 100.0); 
  
        //Prints the total cost of the purchases including tax. 
System.out.println ("The total cost of the purchases with tax is" + " " + (int) (totalCostofPurchasesWithTax * 100) / 100.0);
         
     
                              }
  }

