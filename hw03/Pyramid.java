///////////////////////////////////////
///CSE2 Pyramid 
///This program calculates the volume of pyramid. 

import java.util.Scanner;
 public class Pyramid {
   // main method required for every Java program
   			public static void main(String[] args) {
          //declaring instance of the Scanner object
          Scanner myScanner = new Scanner( System.in ); 
          //prompting user for the length of the square side of the pyramid.  
          System.out.print("The square side of the pyramid is (input length):"); 
          //accepting user input
          double pyramidSquareSide = myScanner.nextDouble();
          //prompt user for the height of the pyramid
          System.out.print("The height of the pyramid is (input height):");
          //accepting user input
          double pyramidHeight = myScanner.nextDouble();
          //calculating the volume of the pyramid
          double Volume = ((pyramidSquareSide * pyramidSquareSide) * pyramidHeight) / 3;
          //printing the volume of the pyramid
          System.out.println("The volume inside the pyramid is" + " " + Volume + ".");
          
          
}  //end of main method   
  	} //end of class
  