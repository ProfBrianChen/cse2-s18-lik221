///////////////////
///CSE2 Convert
///Computes the number of acres of land affected by hurricane precipitation
///Computes the quantity of rain in cubic miles 

import java.util.Scanner; 
// Using a scanner for number of acres of land affected and the quantity of rain 
//
//
public class Convert{
  //main method 
  public static void main(String[] args) {
    //declaring instance of the Scanner object
    Scanner myScanner = new Scanner( System.in ); 
    //prompt user to type in the number of acres affected by hurricane precipitation 
     System.out.print("Enter the affected area in acres(in the form xx.xx):");
     //accepting user input about number of acres 
     double acres = myScanner.nextDouble(); 
     //accepting user input about number of inches of rainfall 
     double rainInches = myScanner.nextDouble();
    System.out.print("Enter the rainfall in the affected area (as a whole number)");
     double rainSquareMiles = (rainInches * rainInches) / 444;
     System.out.println(rainSquareMiles);
     
    
  }
}
