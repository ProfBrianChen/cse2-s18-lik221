import java.util.Random;
import java.util.Scanner;
public class Sentence{
  public static String adjectiveGenerator( String adjective){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
     switch (randomInt){
      case 0:
        adjective = "funny";
      break;
    }
    switch (randomInt){
      case 1:
        adjective = "cool";
      break;
    }
    switch (randomInt){
      case 2:
        adjective = "nice";
      break;
    }
    switch (randomInt){
      case 3:
        adjective = "tasty";
      break;
    }
    switch (randomInt){
      case 4:
        adjective = "delicious";
      break;
    }
    switch (randomInt){
      case 5:
        adjective = "energetic";
      break;
    }
    switch (randomInt){
      case 6:
        adjective = "exciting";
      break;
    }
    switch (randomInt){
      case 7:
        adjective = "handsome";
      break;
    }
    switch (randomInt){
      case 8:
        adjective = "gross";
      break;
    }
    switch (randomInt){
      case 9:
        adjective = "unhappy";
      break;
    }
    return adjective;
  }
  
  public static String firstNounGenerator( String noun1){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
     switch (randomInt){
      case 0:
        noun1 = "dog";
      break;
    }
    switch (randomInt){
      case 1:
        noun1 = "dog";
      break;
    }
    switch (randomInt){
      case 2:
        noun1 = "cat";
      break;
    }
    switch (randomInt){
      case 3:
        noun1 = "man";
      break;
    }
    switch (randomInt){
      case 4:
        noun1 = "woman";
      break;
    }
    switch (randomInt){
      case 5:
        noun1 = "turtle";
      break;
    }
    switch (randomInt){
      case 6:
        noun1 = "baseball";
      break;
    }
    switch (randomInt){
      case 7:
        noun1 = "shirt";
      break;
    }
    switch (randomInt){
      case 8:
        noun1 = "finger";
      break;
    }
    switch (randomInt){
      case 9:
        noun1 = "videogame";
      break;
    }
    return noun1;
  }
    
  public static String secondNounGenerator( String noun2){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
     switch (randomInt){
      case 0:
        noun2 = "nun";
      break;
    }
    switch (randomInt){
      case 1:
        noun2 = "teacher";
      break;
    }
    switch (randomInt){
      case 2:
        noun2 = "priest";
      break;
    }
    switch (randomInt){
      case 3:
        noun2 = "frog";
      break;
    }
    switch (randomInt){
      case 4:
        noun2 = "leg";
      break;
    }
    switch (randomInt){
      case 5:
        noun2 = "phone";
      break;
    }
    switch (randomInt){
      case 6:
        noun2 = "potato";
      break;
    }
    switch (randomInt){
      case 7:
        noun2 = "fish";
      break;
    }
    switch (randomInt){
      case 8:
        noun2 = "shrimp";
      break;
    }
    switch (randomInt){
      case 9:
        noun2 = "pencil";
      break;
    }
    return noun2;
  }
      
  public static String verbGenerator( String verb){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
     switch (randomInt){
      case 0:
        verb = "hit";
      break;
    }
    switch (randomInt){
      case 1:
        verb = "hit";
      break;
    }
    switch (randomInt){
      case 2:
        verb = "punched";
      break;
    }
    switch (randomInt){
      case 3:
        verb = "jumped";
      break;
    }
    switch (randomInt){
      case 4:
        verb = "ate";
      break;
    }
    switch (randomInt){
      case 5:
        verb = "fought";
      break;
    }
    switch (randomInt){
      case 6:
        verb = "ran";
      break;
    }
    switch (randomInt){
      case 7:
        verb = "fell";
      break;
    }
    switch (randomInt){
      case 8:
        verb = "wrote";
      break;
    }
    switch (randomInt){
      case 9:
        verb = "licked";
      break;
    }
    return verb;
  }
  
  public static void main(String[] args) {
    Scanner myScanner1 = new Scanner( System.in );
    int counter = 0;
    while (counter < 1){
      String x = "";
      String y = "";
      String z = "";
      String s = "";
      x = adjectiveGenerator(x);
      y = firstNounGenerator(y);
      z = secondNounGenerator(z);
      s = verbGenerator(s);
    
      System.out.println("The " + adjectiveGenerator(x) + " " + adjectiveGenerator(x) + " " + firstNounGenerator(y) + " " + verbGenerator(s) + " the " + adjectiveGenerator(x) + " " + secondNounGenerator(z) + ". ");
    
    System.out.print("Would you like to generate another sentence? type 1 for yes and 2 for no: ");
      int anotherGame = myScanner1.nextInt();
    if (anotherGame == 1){
      counter=0;
    }
    if (anotherGame == 2){
      System.out.print("Would you like to generate a random paragraph? type 1 for yes and 2 for no: ");
      int paragraph = myScanner1.nextInt();
      if (paragraph ==1){
        ParagraphGenerator();
        break;
      }
      if (paragraph ==2){
        counter++;
      }
}
}
  }
  
  public static void ParagraphGenerator (){
      String x = "";
      String y = "";
      String z = "";
      String s = "";
      x = adjectiveGenerator(x);
      y = firstNounGenerator(y);
      z = secondNounGenerator(z);
      s = verbGenerator(s);
      String a = "a";
    
      System.out.print("The " + adjectiveGenerator(x) + " " + adjectiveGenerator(x) + " " + firstNounGenerator(y) + " " + verbGenerator(s) + " the " + adjectiveGenerator(x) + " " + secondNounGenerator(z) + ". ");
      System.out.print("The " + adjectiveGenerator(x) + " " + adjectiveGenerator(x) + " " + firstNounGenerator(y) + " " + verbGenerator(s) + " the " + adjectiveGenerator(x) + " " + secondNounGenerator(z) + ". ");
      System.out.print("The " + adjectiveGenerator(x) + " " + adjectiveGenerator(x) + " " + firstNounGenerator(y) + " " + verbGenerator(s) + " the " + adjectiveGenerator(x) + " " + secondNounGenerator(z) + ". ");
      System.out.print("The " + adjectiveGenerator(x) + " " + adjectiveGenerator(x) + " " + firstNounGenerator(y) + " " + verbGenerator(s) + " the " + adjectiveGenerator(x) + " " + secondNounGenerator(z) + ". ");
  }
}