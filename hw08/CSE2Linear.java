///Lisa Kestelboym 
/// hw 08, part 1 , CSE2Linear 
///April 12, 2018 
///


import java.util.Scanner;
import java.util.Arrays;


public class CSE2Linear{ //Class 
  
    public static void main (String[]args){ // Main Method
    
        System.out.println("Enter 15 integers in ascending order: ");// prompts user to enter 15 integers
                                           
        Scanner input = new Scanner( System.in );// Scanner
                                           
        int count = 0;// counter for inputs
        int[] myArray = new int[15];// array with a length of 15
        while (count < 15)
        {
            if (input.hasNextInt()) //validates if the user input is of type integer
            {
                myArray[count] = input.nextInt();
              
                if (myArray[count] < 0 || myArray[count] > 100)// second condition checks that the integer is between 0 and 100 
                {
                    System.out.println("Error: not in range of 0 to 100. "); //error message 
                    System.exit(0);
                }
                else// if not continues
                {
                    if (count > 0)// if the counter is greater than 0
                    {
                        if (myArray[count] < myArray[count - 1]) //3rd condition checks to make sure that the user input is greater than the previous integer
                        {
                            System.out.println("Error: Not in the correct order ");// error message
                            System.exit(0);
                        }
                    }
                count++;
                }
            }
            else 
            {
                System.out.println("Error: not an integer");// error message 
                System.exit(0); //ends program 
            }
        }
                                           
                                           
        System.out.println("Sorted: ");                                    
        System.out.println(Arrays.toString(myArray)); //converts array to string
                                           
        System.out.print("Enter a grade to search for: ");
                                           
        if (input.hasNextInt()) 
        {
            int y = input.nextInt();
            BinaryS(myArray, y);
            Scramble(myArray);
            System.out.println("Scrambled:");
            System.out.println(Arrays.toString(myArray));
            System.out.print("Enter a grade to search for: ");
            if (input.hasNextInt())
            {
                LinearS(myArray,y); // calling method 
            }
            else
            {
                System.out.println("Error: not an integer"); //error message 
                System.exit(0);
            }
        }
        else 
        {
            System.out.println("Error: no grade found"); 
            System.exit(0);
        }
    }
    
  
  
    public static void LinearS(int[] myArray, int x)// LinearSearch method
    {
        for (int i = 0; i < myArray.length; i++) 
        {
            if (myArray[i] == x) 
            {
                System.out.println(x + " was found in the list in " + (i + 1) + " iterations"); 
                break;
            }
            else if (myArray[i] != x && i == (myArray.length - 1)) 
            {
                System.out.println(x + " was not found in " + (i + 1) + " iterations");
            }
        }
    }
    public static int[] Scramble(int[] myArray)// Scramble method
    {
        for (int i = 0; i < myArray.length; i++)
        {
            int x = (int)(Math.random()*myArray.length);
            int store = myArray[i];
            while (x != i)
            {
                myArray[i] = myArray[x];
                myArray[x] = store;
                break;
            }
        }
        return myArray;
    }
    public static void BinaryS(int[] array2, int integer)
    {
        int highIndex = array2.length - 1;
        int lowIndex = 0;
        int counter = 0;
        while (lowIndex <= highIndex)
        {
            counter++;
            int midIndex = (highIndex + lowIndex)/2;
            if (integer < array2[midIndex])
            {
                highIndex = midIndex - 1;
            }
            else if (integer > array2[midIndex])
            {
                lowIndex = midIndex + 1;   
            }
            else if (integer == array2[midIndex])
            {
                System.out.println(integer + " was found in " + counter + " iterations");
                break;
            }
        }
        if (lowIndex > highIndex)
        {
            System.out.println(integer + " was not found in " + counter + " iterations");
        }
    }
}