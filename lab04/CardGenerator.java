////////////////////////////////
///Lisa Kestelboym 
///CSE2 - Section 112 
///February 16, 2018 
///Card Generator - program will randomly pick out of a card from a deck of 52 cards. 
public class CardGenerator {
  public static void main(String[] args){
	  //depending on the random number generator, this program will use that number to determine both the suit and rank of the card. 
    String suit = "";
    String rank = "";
    int numberOfCardsPerSuit = 13;
    int card = (int) (Math.random() * 52 + 1)
	  // determining the suit of the card depending on which suit range the card falls under. 
	  // 1-13 is Diamonds
	  // 14-26 is Clubs
	  //27-39 is Hearts 
	  //40-52 is Spades 
    if (card >= 1 && card <= 13 ){
      suit = "Diamonds";
    }
    
    else if (card >= 14 && card <= 26) {
      suit = "Clubs";
    }
    
    else if (card >= 27 && card <= 39) {
      suit = "Hearts";
    }
    
    else if (card >= 40 && card <= 52) {
      suit = "Spades";
    }
   //if statements that determine what rank the card will be depending on the remainder when divided by thirteen. 
    if(card % numberOfCardsPerSuit == 1){
      rank = "Ace";
    }
    else if(card % numberOfCardsPerSuit == 2){
      rank = "Two";
    }
    else if(card % numberOfCardsPerSuit == 3){
      rank = "Three";
    }
    else if(card % numberOfCardsPerSuit == 4){
      rank = "Four";
    }
    else if(card % numberOfCardsPerSuit == 5){
      rank = "Five";
    }
    else if(card % numberOfCardsPerSuit == 6){
      rank = "Six";
    }
    else if(card % numberOfCardsPerSuit == 7){
      rank = "Seven";
    }
    else if(card % numberOfCardsPerSuit == 8){
      rank = "Eight";
    }
    else if(card % numberOfCardsPerSuit == 9){
      rank = "Nine";
    }
    else if(card % numberOfCardsPerSuit == 10){
      rank = "Ten";
    }
    else if(card % numberOfCardsPerSuit == 11){
      rank = "Jack";
    }
    else if(card % numberOfCardsPerSuit == 12){
      rank = "Queen";
    }
    else if(card % numberOfCardsPerSuit == 0){
      rank = "King";
    }
      

	
	//Printing out the card's rank and suit 
      
   System.out.println("Your card is the " + rank + " of " + suit);
  } 
  
} 