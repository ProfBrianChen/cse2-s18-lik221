///////////////////
///Lisa Kestelboym
///CSE2 Hw05 
///March  6, 2018 
///This program uses loops that asks the user to enter information relating to a course they are currently taking with the correct variable type. 

import java.util.Scanner; //importing Scanner 

public class Hw05{ 
  public static void main(String[] args){ 
    
    
    //Asking user for the course number in integer form 
    System.out.println("What is the course number of the course you are currently taking? ");
    
    int courseNumber = 0; //stating course number integer variable 
    //introducing scanner for the courseNumber variable input 
    Scanner courseScanner = new Scanner( System.in );
    while (true){
      if (courseScanner.hasNextInt() == false){ //if the input is not in integer form
        System.out.println("Invalid input. Enter an integer: ");
        String junkWord = courseScanner.next(); //system deletes user input and prompts user for new input 
      }
      else if (courseScanner.hasNextInt() == true){ //when the input is in integer form, input is taken in 
        courseNumber = courseScanner.nextInt();
        break;
      }
    }
    
    // all future input will include the same steps, only varying in variable type/name of the variable 
    
    
    //taking in input for the department name of the course 
    //requiring that the user inputs in string form 
    Scanner departmentScanner = new Scanner ( System.in );
    System.out.println("What is the department name of the course?");
    String departmentName; 
    while (true){
      if (departmentScanner.hasNextLine() == false){
        System.out.println("Invalid input. Enter a string value: ");
        String junkWord = departmentScanner.next(); 
      }
      else if (departmentScanner.hasNextLine() == true){
        departmentName = departmentScanner.nextLine(); 
        break;
      }
    }
    
    //taking in input for the number of times in a week that the course meets 
    Scanner meetingScanner = new Scanner ( System.in ); 
    System.out.println("How many times a week does this course meet?");
    int meetingsPerWeek = 0; 
    while (true){ 
      if (meetingScanner.hasNextInt() == false){
        System.out.println("Invalid input. Enter an integer: "); 
        String junkWord = meetingScanner.next(); 
      }
      else if (meetingScanner.hasNextInt() == true){
        meetingsPerWeek = meetingScanner.nextInt(); 
        break; 
      }
    }
    
    //taking in input for the time that the course meets 
    //hour and minute value will be taken in separately
    
    Scanner hourScanner = new Scanner ( System.in ); 
    System.out.println("What time does this course meet?");
    System.out.println("Enter hour: ");
    //requesting input for hour 
    int hour = 0; 
    while (true){ 
      if (hourScanner.hasNextInt() == false){
        System.out.println("Invalid input. Enter an integer: "); 
        String junkWord = hourScanner.next(); 
      }
      else if (hourScanner.hasNextInt() == true){
        hour = hourScanner.nextInt(); 
        break; 
      }
    }
    
    //requesting input for minute 
    Scanner minuteScanner = new Scanner ( System.in ); 
    System.out.println("Enter minute: ");
    int minute = 0; 
    while (true){ 
      if (minuteScanner.hasNextInt() == false){
        System.out.println("Invalid input. Enter an integer: "); 
        String junkWord = minuteScanner.next(); 
      }
      else if (minuteScanner.hasNextInt() == true){
        minute = minuteScanner.nextInt(); 
        break; 
      }
    }
    
    //taking in input for the instructor's name (in string form)
    Scanner instructorScanner = new Scanner ( System.in ); 
    System.out.println("What is your instructor's name?");
    String instructorName; 
    while (true){ 
      if (instructorScanner.hasNextLine() == false){
        System.out.println("Invalid input. Enter a string value: "); 
        String junkWord = instructorScanner.next(); 
      }
      else if (instructorScanner.hasNextLine() == true){
        instructorName = instructorScanner.nextLine(); 
        break; 
      }
    }
    
    //taking in input for the number of students in the class 
    Scanner studentScanner = new Scanner ( System.in ); 
    System.out.println("How many students are in your class?");
    int students = 0; 
    while (true){ 
      if (studentScanner.hasNextInt() == false){
        System.out.println("Invalid input. Enter an integer: "); 
        String junkWord = studentScanner.next(); 
      }
      else if (studentScanner.hasNextInt() == true){
        students = studentScanner.nextInt(); 
        break; 
      }
    }
    
    //printing out all the information that has been taken in by the user 
    System.out.println("Course number: " + courseNumber);
    System.out.println("Department name: " + departmentName);
    System.out.println("This class meets " + meetingsPerWeek + " times a week."); 
    System.out.println("This course meets at " + hour + ":" + minute + ".");
    System.out.println("Instructor's name: " + instructorName);
    System.out.println("There are " + students + " in your class."); 
    
  }
}