////////////////////////////////
///Lisa Kestelboym 
///CSE2 - Section 112 
///February 9, 2018 
///Check- uses the Scanner class to obtain the original cost of the check, the percentage tip, and number of ways the check will be split.
///Determines how much each person in the group needs to spend in order to pay the check.

import java.util.Scanner;
// Using a scanner for original cost, percentage tip, number of ways check will be split, how much each person spends 
//
//
public class Check{
    			// main method required for every Java program
   			public static void main(String[] args) {
          //declaring instance of the Scanner object
          Scanner myScanner = new Scanner( System.in ); 
          //prompting user for the original cost of the check 
          System.out.print("Enter the original cost of the check in the form xx.xx: "); 
          //accepting user input
          double checkCost = myScanner.nextDouble();
          //prompt user for the percentage of the tip they want to pay and accept their input 
          System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx):");
          double tipPercent = myScanner.nextDouble();
          //Converting the percentage into a decimal value
          tipPercent /= 100; 
          //prompt user for number of people who went to dinner and accept input
          System.out.print("Enter the number of people who went out to dinner:");
          int numPeople = myScanner.nextInt();
          double totalCost;
          double costPerPerson;
          //whole dollar amount of cost
          int dollars;
          //for storing digits to the right of the decimal point 
          int dimes; 
          int pennies; 
          //for the cost 
          totalCost = checkCost * (1 + tipPercent);
          //calculating cost per person 
          costPerPerson = totalCost / numPeople;
          //get the whole amount, dropping decimal fraction
          dollars=(int)costPerPerson;
          //get dimes amount, e.g., 
          // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
          //  where the % (mod) operator returns the remainder
          //  after the division:   583%100 -> 83, 27%5 -> 2 
          dimes=(int)(costPerPerson * 10) % 10;
          pennies=(int)(costPerPerson * 100) % 10;
          System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);




}  //end of main method   
  	} //end of class
  