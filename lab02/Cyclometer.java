////////////////////////////////
///Lisa Kestelboym 
///CSE2 - Section 112 
///February 2, 2018 
///Cyclometer- bicycle cyclometer designed to measure time elapsed in seconds and number of rotations of the front wheel in the elapsed time. 
//
public class Cyclometer {
    	//
   	public static void main(String[] args) {
    //computes time elapsed in seconds and number of rotations in that time 
   

	   	int secsTrip1=480;  // number of seconds for Trip 1 
       	int secsTrip2=3220;  // number of seconds for Trip 2 
		int countsTrip1=1561;  // number of wheel rotations for Trip 1 
		int countsTrip2=9037; // number of wheel rotations for Trip 2 
      double wheelDiameter=27.0,  // diameter of the wheel 
  	PI=3.14159, // value of pi 
  	feetPerMile=5280,  // number of feet per mile
  	inchesPerFoot=12,   // number of inches per foot 
  	secondsPerMinute=60;  // number of seconds per minute
	double distanceTrip1, distanceTrip2,totalDistance;  // total distance of both Trip 1 and Trip 2 
      System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts."); //Prints "Trip 1 took ____ minutes and had ____ counts" to the terminal window.
                                       //(the seconds of the trip is divided by 60)
                                       //counts per trip variable is unmodified 
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts."); //Prints "Trip 2 took ____ minutes and had ____ counts" to the terminal window. (same process as above with Trip 1)
      distanceTrip1=countsTrip1*wheelDiameter*PI;
// Above gives distance of Trip 1 in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
distanceTrip1/=inchesPerFoot*feetPerMile;
      // Gives distance in miles
distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
      //Gives distance of trip 2 in miles 
	totalDistance=distanceTrip1+distanceTrip2;
//Print out the output data.
           System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");



	}  
} 


