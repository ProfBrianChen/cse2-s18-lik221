/////////////////////////////////////////////
///CSE2 Yahtzee
///This program simulates the game, Yahtzee.  
import java.util.Scanner; 
public class Yahtzee {
    	//
   	public static void main(String[] args){
      //declaring my scanner and asking user to choose whether to roll the dice randomly or select the die's values. 
      Scanner myScanner = new Scanner( System.in );
      System.out.print("Type '1' to roll the dice randomly or '2' to type in a five-digit number that corresponds to a specific roll: ");
      //casting the user's choice as an integer
      int choiceofRoll = myScanner.nextInt();
      //if user chooses 1, the die will roll randomly 5 times from numbers 1 to 6. 
      if (choiceofRoll == 1){
        //prompting user to type in "1" so that the dice will be rolled randomly from 1 to 6. 
              System.out.println ("Type in 1 to roll your dice five times randomly.");
              int FirstRoll = myScanner.nextInt();
              int roll1 = (int) (Math.random() * 6) + 1;
              int roll2 = (int) (Math.random() * 6) + 1; 
              int roll3 = (int) (Math.random() * 6) +1; 
              int roll4 = (int) (Math.random() * 6) +1; 
              int roll5 = (int) (Math.random() * 6) +1; 
        //printing out the five results from the random rolls. 
              System.out.println("Your rolls: " + roll1 + ", " + roll2 + ", " + roll3 + ", " + roll4 + ", " + roll5); 
        //creating the integer variable that will count the amount of times that dice land on numbers 1 to 6. 
              int countOne = 0;
              int countTwo = 0;
              int countThree = 0;
              int countFour = 0;
              int countFive = 0;
              int countSix = 0;

        //aces score for random rolls - only counting when dice rolls a 1 
              if (roll1 == 1){
                 countOne++;
              } 
              if (roll2 == 1){
                 countOne++;
              } 
              if (roll3 == 1){
                 countOne++;
              } 
              if (roll4 == 1){
                 countOne++;
              }
              if (roll5 ==1){
                 countOne++;
              }
        //printing out aces score 
        System.out.println("Aces score: " + countOne);
        
        //twos score for random rolls - add value of 2 for every 2 that is rolled 
              if(roll1 == 2){
                 countTwo++;
              } 
              if (roll2 == 2){
                 countTwo++;
              } 
              if (roll3 == 2){
                 countTwo++;
              } 
              if (roll4 == 2){
                 countTwo++;
              } 
              if (roll5 == 2){
                 countTwo++;
              } 
        //printing out the two's score 
              System.out.println("Two's score: " + countTwo*2);
        
        
        //three's score for random rolls - counting everytime a 3 is rolled 
              if (roll1 == 3){
                 countThree++;
              } 
              if (roll2 == 3){
                 countThree++;
              } 
              if (roll3 == 3){
                 countThree++;
              } 
              if (roll4 == 3){
                 countThree++;
              } 
              if (roll5 == 3){
                 countThree++;
              } 
        //printing out three's score
        System.out.println("Three's score: " + countThree*3);

         //four's score for random rolls - counting everytime a 4 is rolled 
              if (roll1 == 4){
                 countFour++;
              } 
              if (roll2 == 4){
                 countFour++;
              } 
              if (roll3 == 4){
                 countFour++;
              } 
              if (roll4 == 4){
                 countFour++;
              } 
              if (roll5 == 4){
                 countFour++;
              } 
        //printing out four's score
        System.out.println("Four's score: " + countFour*4); 
        
        
        //five's score for random rolls - counting everytime a five is rolled 
              if (roll1 == 5){
                 countFive++;
              } 
              if (roll2 == 5){
                 countFive++;
              } 
              if (roll3 == 5){
                 countFive++;
              } 
              if (roll4 == 5){
                 countFive++;
              } 
              if (roll5 == 5){
                 countFive++;
              } 
        //printing out five's score 
        System.out.println("Five's score: " + countFive*5); 
        
        //six's score for random rolls - counting everytime a 6 is rolled 
          
              if (roll1 == 6){
                 countSix++;
              } 
              if (roll2 == 6){
                 countSix++;
              } 
              if (roll3 == 6){
                 countSix++;
              } 
              if (roll4 == 6){
                 countSix++;
              } 
              if (roll5 == 6){
                 countSix++;
              } 
        //printing out six's score 
              System.out.println("Six's score: " + countSix*6);
        
        
         //upper score calculation 
              int upperScoreRandom = countOne + countTwo*2 + countThree*3 + countFour*4 + countFive*5 + countSix*6; 
        
        //printing out total upper score with bonus condition - bonus only given in upper score is 63 points or higher 
             int Bonus = 0;
            if (upperScoreRandom > 63){
              Bonus = 35;
            }
            else{
              Bonus = 0;
            }
            int totalUpperScoreRandom = upperScoreRandom + Bonus; 
              System.out.println("Bonus: " + Bonus);
              System.out.println("Your total upper score is: " + totalUpperScoreRandom);
        
        //computing lower score 
        //computing three of a kind score 
            int threeofakindValue = 0;
        //variable for amount of occurances of three of a kind situations for full house calculation later. 
            int threeofakindOccurance = 0; 
            
            if (countOne == 3){
               threeofakindValue = 3;
               threeofakindOccurance++;
            }
            if (countTwo == 3){
              threeofakindValue = 6;
              threeofakindOccurance++;
            }
            if (countThree == 3){
              threeofakindValue = 9;
              threeofakindOccurance++;
            }
            if (countFour == 3){
              threeofakindValue = 12;
              threeofakindOccurance++;
            }
            if (countFive == 3){
              threeofakindValue = 15;
              threeofakindOccurance++;
            }
            if (countSix == 3){
              threeofakindValue = 18;
              threeofakindOccurance++;
            }
        
        //showing that if the different counts do not amount to 3 occurances, the value of the three of a kind score will remain zero. 
            if (countOne != 3 && countTwo != 3 && countThree != 3 && countFour != 3 && countFive != 3 && countSix != 3){
              threeofakindValue = 0;
            }
        //Printing  out three of a kind score
            System.out.println("Three of a Kind Score: " + threeofakindValue);
        
        
        //computing four-of-a-king score
        //computing each possibility four of a kind situation and their count values. 
        int fourofakindValue = 0; 
         if (countOne == 4){
              fourofakindValue = 4;
            }
            if (countTwo == 4){
              fourofakindValue = 8;
            }
            if (countThree == 4){
              fourofakindValue = 12;
            }
            if (countFour == 4){
              fourofakindValue = 16;
            }
            if (countFive == 4){
              fourofakindValue = 20;
            }
            if (countSix == 4){
              fourofakindValue = 24;
            }
            if (countOne != 4 && countTwo != 4 && countThree != 4 && countFour != 4 && countFive != 4 && countSix != 4){
             fourofakindValue = 0;
            }
            
        //Printing out four of a kind solution
            System.out.println("Four of a Kind Score: " + fourofakindValue);
           
        //computing full house score 
        //variable for full house score 
            int FullHouse = 0; 
        //creating variable for matching dice (required for full house as well as three of a kind)
            int MatchingDice = 0; 
            if (countOne == 2){ 
               MatchingDice++;
            }
            if (countTwo == 2){
               MatchingDice++;
            }
            if (countThree == 2){
               MatchingDice++;
            }
            if (countFour == 2){
               MatchingDice++;
            }
            if (countFive ==2){
               MatchingDice++;
            }
            if (countSix == 2){
               MatchingDice++;
            }

      //setting conditions for full house to occur 
            if (MatchingDice == 1 && threeofakindOccurance == 1){
               FullHouse = 25; 
            }
            else {
               FullHouse = 0; 
            }
        
     //printing full house score 
        System.out.println("Full House Score: " + FullHouse); 
        
     //small straight calculations
     //variable for small straight score 
            int SmallStraightScore = 0;
            if((countOne > 0 && countTwo > 0 & countThree > 0 && countFour > 0) || (countTwo > 0 && countThree > 0 && countFour > 0 && countFive > 0) || (countThree > 0 && countFour > 0 && countFive > 0 && countSix > 0)){
              SmallStraightScore = 30;
            }
            else{
             SmallStraightScore = 0;
            }
            //printing small straight solution
            System.out.println("Small Straight Score: " + SmallStraightScore);
        
        //large straight calculations 
        //variable for large straight score 
            int LargeStraightScore = 0; 
            if((countOne > 0 && countTwo > 0 && countThree > 0 && countFour > 0 && countFive > 0) || (countTwo > 0 && countThree > 0 && countFour > 0 && countFive > 0 && countSix > 0)){
              LargeStraightScore = 40;
            }
            else{
              LargeStraightScore = 0;
            }
        //print large straight calculation
            System.out.println("Large Straight Score: " + LargeStraightScore);
        
        //yahtzee calculation 
        //if all five die are the same, then it is yahtzee
            int Yahtzee = 0;
            if (countOne == 5 || countTwo == 5 || countThree == 5 || countFour == 5 || countSix == 5){
              Yahtzee = 50;
            }
            else{
              Yahtzee = 0;
            }
          
        //print yahtzee solution
            System.out.println("Yahtzee: " + Yahtzee);
          
        //chance calculation - sum of all five die values 
            int Chance= roll1 + roll2 + roll3 + roll4 + roll5;
            System.out.println("Chance score:" + Chance);
        
        //computing and printing total lower section score. 
            int totalLowerSectionRandom = threeofakindValue + fourofakindValue + FullHouse + SmallStraightScore + LargeStraightScore + Yahtzee + Chance;
            System.out.println("Total Lower Section: " + totalLowerSectionRandom);
            System.out.println("Upper Section: " + totalUpperScoreRandom);
        //computing and printing grand total of game, which is the lower and upper score added together. 
            int GrandTotalRandom = totalUpperScoreRandom + totalLowerSectionRandom;
            System.out.println("Grand Total: " + GrandTotalRandom);
        }
      
        
              else if (choiceofRoll == 2){
        //prompting user to type in "1" so that the dice will be rolled randomly from 1 to 6. 
        //all variables will be identical except they will be tagged with 'Sel' or 'Selected' at the end so that user knows that these variables are from the selected option of the game 
              System.out.println ("Type in a number from 1 - 6 and press 'enter'. Repeat 5 times.");
              int roll1Sel = myScanner.nextInt();
        //System exits if any of the selected roll inputs are not a number from 1 to 6. 
              if (roll1Sel > 6 || roll1Sel < 1){
                System.out.println("Selected roll is not valid. Needs to be a number from 1-6.");
                System.exit(0);
              }
              int roll2Sel = myScanner.nextInt();
              if (roll2Sel > 6 || roll2Sel < 1){
                System.out.println("Selected roll is not valid. Needs to be a number from 1-6.");
                System.exit(0);
              }
              int roll3Sel = myScanner.nextInt();
              if (roll3Sel > 6 || roll3Sel < 1){
                System.out.println("Selected roll is not valid. Needs to be a number from 1-6.");
                System.exit(0);
              }
              int roll4Sel = myScanner.nextInt();
              if (roll4Sel > 6 || roll4Sel < 1){
                System.out.println("Selected roll is not valid. Needs to be a number from 1-6.");
                System.exit(0);
              }
              int roll5Sel = myScanner.nextInt();
              if (roll5Sel > 6 || roll5Sel < 1){
                System.out.println("Selected roll is not valid. Needs to be a number from 1-6.");
                System.exit(0);
              }
              
                
        //printing out the five results from the selected  rolls. 
              System.out.println("Your rolls: " + roll1Sel + ", " + roll2Sel + ", " + roll3Sel + ", " + roll4Sel + ", " + roll5Sel); 
        //creating the integer variable that will count the amount of times that dice land on numbers 1 to 6. 
              int countOneSel = 0;
              int countTwoSel = 0;
              int countThreeSel = 0;
              int countFourSel = 0;
              int countFiveSel = 0;
              int countSixSel = 0;
              
        //aces score for selected rolls - only counting when dice rolls a 1 
              if (roll1Sel == 1){
                 countOneSel++;
              } 
              if (roll2Sel == 1){
                 countOneSel++;
              } 
              if (roll3Sel == 1){
                 countOneSel++;
              } 
              if (roll4Sel == 1){
                 countOneSel++;
              }
              if (roll5Sel ==1){
                 countOneSel++;
              }
        //printing out aces score 
        System.out.println("Aces score: " + countOneSel);
        
        //twos score for selected rolls - add value of 2 for every 2 that is rolled 
              if(roll1Sel == 2){
                 countTwoSel++;
              } 
              if (roll2Sel == 2){
                 countTwoSel++;
              } 
              if (roll3Sel == 2){
                 countTwoSel++;
              } 
              if (roll4Sel == 2){
                 countTwoSel++;
              } 
              if (roll5Sel == 2){
                 countTwoSel++;
              } 
        //printing out the two's score 
              System.out.println("Two's score: " + countTwoSel*2);
        
        
        //three's score for selected rolls - counting everytime a 3 is rolled 
              if (roll1Sel== 3){
                 countThreeSel++;
              } 
              if (roll2Sel == 3){
                 countThreeSel++;
              } 
              if (roll3Sel == 3){
                 countThreeSel++;
              } 
              if (roll4Sel == 3){
                 countThreeSel++;
              } 
              if (roll5Sel == 3){
                 countThreeSel++;
              } 
        //printing out three's score
        System.out.println("Three's score: " + countThreeSel*3);

         //four's score for selected  rolls - counting everytime a 4 is rolled 
              if (roll1Sel == 4){
                 countFourSel++;
              } 
              if (roll2Sel == 4){
                 countFourSel++;
              } 
              if (roll3Sel == 4){
                 countFourSel++;
              } 
              if (roll4Sel == 4){
                 countFourSel++;
              } 
              if (roll5Sel == 4){
                 countFourSel++;
              } 
        //printing out four's score
        System.out.println("Four's score: " + countFourSel*4); 
        
        
        //five's score for selected rolls - counting everytime a five is rolled 
              if (roll1Sel == 5){
                 countFiveSel++;
              } 
              if (roll2Sel == 5){
                 countFiveSel++;
              } 
              if (roll3Sel == 5){
                 countFiveSel++;
              } 
              if (roll4Sel == 5){
                 countFiveSel++;
              } 
              if (roll5Sel == 5){
                 countFiveSel++;
              } 
        //printing out five's score 
        System.out.println("Five's score: " + countFiveSel*5); 
        
        //six's score for selected rolls - counting everytime a 6 is rolled 
          
              if (roll1Sel == 6){
                 countSixSel++;
              } 
              if (roll2Sel == 6){
                 countSixSel++;
              } 
              if (roll3Sel == 6){
                 countSixSel++;
              } 
              if (roll4Sel == 6){
                 countSixSel++;
              } 
              if (roll5Sel == 6){
                 countSixSel++;
              } 
        //printing out six's score 
              System.out.println("Six's score: " + countSixSel*6);
        
        
         //upper score calculation 
              int upperScoreSelected = countOneSel + countTwoSel*2 + countThreeSel*3 + countFourSel*4 + countFiveSel*5 + countSixSel*6; 
        
        //printing out total upper score with bonus condition - bonus only given in upper score is 63 points or higher 
             int BonusSel = 0;
            if (upperScoreSelected > 63){
              BonusSel = 35;
            }
            else{
              BonusSel = 0;
            }
            int totalUpperScoreSelected = upperScoreSelected + BonusSel; 
              System.out.println("Bonus: " + BonusSel);
              System.out.println("Your total upper score is: " + totalUpperScoreSelected);
        
        //computing lower score 
        //computing three of a kind score 
            int threeofakindValueSel = 0;
        //variable for amount of occurances of three of a kind situations for full house calculation later. 
            int threeofakindOccuranceSel = 0; 
            
            if (countOneSel == 3){
               threeofakindValueSel = 3;
               threeofakindOccuranceSel++;
            }
            if (countTwoSel == 3){
              threeofakindValueSel = 6;
              threeofakindOccuranceSel++;
            }
            if (countThreeSel == 3){
              threeofakindValueSel = 9;
              threeofakindOccuranceSel++;
            }
            if (countFourSel == 3){
              threeofakindValueSel = 12;
              threeofakindOccuranceSel++;
            }
            if (countFiveSel == 3){
              threeofakindValueSel = 15;
              threeofakindOccuranceSel++;
            }
            if (countSixSel == 3){
              threeofakindValueSel = 18;
              threeofakindOccuranceSel++;
            }
        
        //showing that if the different counts do not amount to 3 occurances, the value of the three of a kind score will remain zero. 
            if (countOneSel != 3 && countTwoSel != 3 && countThreeSel != 3 && countFourSel != 3 && countFiveSel != 3 && countSixSel != 3){
              threeofakindValueSel = 0;
            }
        //Printing  out three of a kind score
            System.out.println("Three of a Kind Score: " + threeofakindValueSel);
        
        
        //computing four-of-a-king score
        //computing each possibility four of a kind situation and their count values. 
        int fourofakindValueSel = 0; 
         if (countOneSel == 4){
              fourofakindValueSel = 4;
            }
            if (countTwoSel == 4){
              fourofakindValueSel = 8;
            }
            if (countThreeSel == 4){
              fourofakindValueSel = 12;
            }
            if (countFourSel == 4){
              fourofakindValueSel = 16;
            }
            if (countFiveSel == 4){
              fourofakindValueSel = 20;
            }
            if (countSixSel == 4){
              fourofakindValueSel = 24;
            }
            if (countOneSel != 4 && countTwoSel != 4 && countThreeSel != 4 && countFourSel != 4 && countFiveSel != 4 && countSixSel != 4){
             fourofakindValueSel = 0;
            }
            
        //Printing out four of a kind solution
            System.out.println("Four of a Kind Score: " + fourofakindValueSel);
           
        //computing full house score 
        //variable for full house score 
            int FullHouseSel = 0; 
        //creating variable for matching dice (required for full house as well as three of a kind)
            int MatchingDiceSel = 0; 
            if (countOneSel == 2){ 
               MatchingDiceSel++;
            }
            if (countTwoSel == 2){
               MatchingDiceSel++;
            }
            if (countThreeSel == 2){
               MatchingDiceSel++;
            }
            if (countFourSel == 2){
               MatchingDiceSel++;
            }
            if (countFiveSel ==2){
               MatchingDiceSel++;
            }
            if (countSixSel == 2){
               MatchingDiceSel++;
            }

      //setting conditions for full house to occur 
            if (MatchingDiceSel == 1 && threeofakindOccuranceSel == 1){
               FullHouseSel = 25; 
            }
            else {
               FullHouseSel = 0; 
            }
        
     //printing full house score 
        System.out.println("Full House Score: " + FullHouseSel); 
        
     //small straight calculations
     //variable for small straight score 
            int SmallStraightScoreSel = 0;
            if((countOneSel > 0 && countTwoSel > 0 & countThreeSel > 0 && countFourSel > 0) || (countTwoSel > 0 && countThreeSel > 0 && countFourSel > 0 && countFiveSel > 0) || (countThreeSel > 0 && countFourSel > 0 && countFiveSel > 0 && countSixSel > 0)){
              SmallStraightScoreSel = 30;
            }
            else{
             SmallStraightScoreSel = 0;
            }
            //printing small straight solution
            System.out.println("Small Straight Score: " + SmallStraightScoreSel);
        
        //large straight calculations 
        //variable for large straight score 
            int LargeStraightScoreSel = 0; 
            if((countOneSel > 0 && countTwoSel > 0 && countThreeSel > 0 && countFourSel > 0 && countFiveSel > 0) || (countTwoSel > 0 && countThreeSel > 0 && countFourSel > 0 && countFiveSel > 0 && countSixSel > 0)){
              LargeStraightScoreSel = 40;
            }
            else{
              LargeStraightScoreSel = 0;
            }
        //print large straight calculation
            System.out.println("Large Straight Score: " + LargeStraightScoreSel);
        
        //yahtzee calculation 
        //if all five die are the same, then it is yahtzee
            int YahtzeeSel = 0;
            if (countOneSel == 5 || countTwoSel == 5 || countThreeSel == 5 || countFourSel == 5 || countSixSel == 5){
              YahtzeeSel = 50;
            }
            else{
              YahtzeeSel = 0;
            }
          
        //print yahtzee solution
            System.out.println("Yahtzee: " + YahtzeeSel);
          
        //chance calculation - sum of all five die values 
            int ChanceSel= roll1Sel + roll2Sel + roll3Sel + roll4Sel + roll5Sel;
            System.out.println("Chance score:" + ChanceSel);
        
        //computing and printing total lower section score. 
            int totalLowerSectionSelected = threeofakindValueSel + fourofakindValueSel + FullHouseSel + SmallStraightScoreSel + LargeStraightScoreSel + YahtzeeSel + ChanceSel;
            System.out.println("Total Lower Section: " + totalLowerSectionSelected);
            System.out.println("Upper Section: " + totalUpperScoreSelected);
        //computing and printing grand total of game, which is the lower and upper score added together. 
            int GrandTotalSelected = totalUpperScoreSelected + totalLowerSectionSelected;
            System.out.println("Grand Total: " + GrandTotalSelected);
              }
     


      }

    
}