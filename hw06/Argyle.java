///////////////////
///Lisa Kestelboym
///CSE2 Hw06 
///March  20, 2018 
///This program takes the user's input for measurements and characters used in order to create an argyle pattern.  

import java.util.Scanner; //importing Scanner 

public class Argyle{ 
  public static void main(String[] args){ 
    
    //introducing scanner for the user variable input 
    Scanner myScanner = new Scanner( System.in );
    
    //Asking user for the width of the viewing window of the pattern  
    System.out.print("Please enter a positive integer for the width of the viewing window in characters: ");
    int width = myScanner.nextInt(); //declaring width as next integer variable 
    
    System.out.print("Please enter a positive integer for the height of the viewing window in characters: ");
    int height = myScanner.nextInt(); //declaring height as next integer variable 
    
    System.out.print("Please enter a positive integer for the width of the argyle diamonds: ");
    int widthOfArgyleDiamond = myScanner.nextInt(); 
    
    System.out.print("Please enter a positive odd integer for the width of the argyle center stripe: ");
    int widthOfCenterStripe = myScanner.nextInt(); 
    
    System.out.print("Please enter a first character for the pattern fill: ");
    char firstCharacter = myScanner.next().charAt(0);
    
    System.out.print("Please enter a second character for the pattern fill: ");
    char secondCharacter = myScanner.next().charAt(0);
    
    System.out.print("Please enter a third character for the pattern fill: ");
    char thirdCharacter = myScanner.next().charAt(0);
    
    while (widthOfCenterStripe == 1){
      System.out.println (thirdCharacter);
      if (widthOfCenterStripe != 1){
        break; 
    }
    }
    
    while  (widthOfCenterStripe > 1){
      System.out.print(thirdCharacter);
      widthOfCenterStripe--;
      if (widthOfCenterStripe <= 1){
        break;
      }
    }
    
    while (widthOfArgyleDiamond != 0){
      System.out.print(firstCharacter);
      widthOfArgyleDiamond--;
      if (widthOfArgyleDiamond == 0){
      break;
    }
    }
    
     widthOfCenterStripe++;
    
    while (widthOfCenterStripe != 0){
      System.out.print(thirdCharacter);
      widthOfCenterStripe--;
      if (widthOfCenterStripe == 0){
      break;
    }
    }
   
  }
}
