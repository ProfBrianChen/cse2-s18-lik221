///////////////////
///CSE2 WelcomeClass 
///
  public class WelcomeClass {
    
      public static void main(String[] args) {
            //Prints "Welcome Class" and "LIK221" to the terminal window. 
            System.out.println("  -----------");
            System.out.println("  | WELCOME |");
            System.out.println("  -----------");
            System.out.println("  ^  ^  ^  ^  ^  ^");
            System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
            System.out.println("<-L--I--K--2--2--1->");   
            System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
            System.out.println("  v  v  v  v  v  v");
            System.out.println("My name is Lisa Kestelboym and I am a first-generation Russian who lives in New Jersey.");
        
                              }
  }
